package com.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialDuaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialDuaApplication.class, args);
	}
}
