package com.tutorial;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PageController {
	@RequestMapping("/hello")
	public String index() {
		return "hello";
	}

	@RequestMapping("/greeting")
	public String greeting(@RequestParam(value = "name", required = false, defaultValue = "dunia") String name, Model model) {
		model.addAttribute("name", name);
		return "greeting";
	}
	
	@RequestMapping("/jumlah")
	public String jumlah(@RequestParam(value = "a", defaultValue = "0") int angkaPertama, @RequestParam(value = "b", defaultValue = "0") int angkaKedua, Model model) {
		model.addAttribute("a", angkaPertama);
		model.addAttribute("b", angkaKedua);
		return "jumlah";
	}
}